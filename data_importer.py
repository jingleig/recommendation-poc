import json
from urllib import request, parse
import os
import subprocess


def import_hf_data():
    urlbase = 'https://internal-aem-publisher.ef.cn/apps/adam/course.json/content/adam/courses/'
    activity_urlbase = 'http://10.163.8.4:4503/apps/adam/activity.off.json/content/adam/courses/'

    for i in range(1, 8):
        url = urlbase + "highflyers/book-" + str(i)
        cmd = f"""curl {url} | jq -r '.units[].midTerm | select(. != null) | .reading, .grammar, .vocabulary, .listening | .[] |.path'"""
        mid_term_exams = subprocess.check_output(cmd, shell=True, text=True).strip()

        for mid_term_exam in mid_term_exams.splitlines():
            mid_term_exam_url = activity_urlbase + mid_term_exam
            with request.urlopen(mid_term_exam_url) as response:
                body = json.loads(response.read().decode())
                try:
                    activityKey = body['Key']
                    activityTitle = body['Title']
                    activityType = body['Type']
                    for question in body['Questions']:
                        item = {}
                        questionKey = question['key']
                        tags = question['body']['tags']
                        item['activityKey'] = activityKey
                        item['activityKeyRaw'] = activityKey
                        item['activityTitle'] = activityTitle
                        item['activityType'] = activityType
                        item['questionKey'] = questionKey
                        item['questionKeyRaw'] = questionKey
                        origin_tags = {}
                        for tag in tags:
                            origin_tags.update(tag)
                        item['originTags'] = origin_tags
                        plain_tags = []
                        for tag_group in tags:
                            for k, tags_in_group in tag_group.items():
                                for tag_in_group in tags_in_group:
                                    plain_tags.append(tag_in_group)
                        item['tags'] = plain_tags
                        # print(json.dumps(item, indent = 2))
                        sendToEs(item)
                except KeyError as error:
                    print(body)

def sendToEs(question: dict):
    es_url = 'http://localhost:9200'
    index_name = 'recommendation'
    type_name = 'question'
    url = es_url + '/' + index_name + '/' + type_name
    data = json.dumps(question)
    req = request.Request(url, data=bytes(data.encode("utf-8")), method='POST')
    req.add_header('Content-Type', 'application/json')
    try:
        with request.urlopen(req) as response:
            print(response.read().decode())
    except Exception as error:
        print(error)

if __name__ == "__main__":
    import_hf_data()