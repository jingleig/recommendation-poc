curl --location --request GET 'http://localhost:9200/recommendation/question/_search?pretty=true' \
--header 'Content-Type: application/json' \
--data-raw '{
    "query": {
        "bool": {
            "must": [
                // primary-skill-set
                {
                    "term": {
                        "tags": {
                            "value": "K-AND-T:SKILLSET/VOCABULARY"
                        }
                    }
                }
            ],
            "should": [
                // picked from activity title, boost up to 2.0
                {
                    "match": {
                        "activityTitle": {
                            "query": "Vocabulary C",
                            "boost": 2.0
                        }
                    }
                },
                // picked from primary-sub-skill, boost up to 2.0
                // {
                //     "term": {
                //         "tags": {
                //             "value": "K-AND-T:SUB-SKILLS/READING/SK-XX-REA-3",
                //             "boost" : 2.0
                //         }
                //     }
                // },
                // picked from primary-lexical-set boost 0.8
                {
                    "term": {
                        "tags": {
                            "value": "COMPASS:TO/XX/HHE/4/3",
                            "boost": 0.8
                        }
                    }
                },
                {
                    "term": {
                        "tags": {
                            "value": "COMPASS:TO/XX/HHE/1/3",
                            "boost": 0.8
                        }
                    }
                }
                // secondary-skill-set boost 1
                // secondary-lexical-set boost 0.6
                // more tags needed here to make more precise recommendation lower the boost to 0.5
            ],
            "must_not": [
                {
                    // exclude the activity used by searching
                    "term": {
                        "activityKeyRaw": "highflyers/book-2/unit-3/activities/midterm/matchaudiotoimage-vocabularya"
                    }
                }
            ],
            // filter with the diffculty and also using the cache in elasticsearch
            "filter": [
                // same CEFR
                {
                    "term": {
                        "tags": "CEFR:A1.1."
                    }
                },
                // same book and unit
                {
                    "prefix": {
                        "activityKeyRaw": "highflyers/book-2/unit-3"
                    }
                }
            ]
        }
    },
    // only need activityKeyRaw field
    "_source": [
        "activityKeyRaw"
    ],
    "collapse": {
        "field": "activityKeyRaw"
    }
}'