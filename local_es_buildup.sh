docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elasticsearch docker.elastic.co/elasticsearch/elasticsearch:7.10.1

while true
do
    curl localhost:9200
    UP=$?
    if [ $UP -eq 0 ]
    then
        echo "ES UP"
        break
    else
        echo "ES NOT UP"
        sleep 2
    fi
done

# create index
curl --location --request PUT 'localhost:9200/recommendation'
# put mapping
curl --location --request PUT 'localhost:9200/recommendation/question/_mapping?include_type_name=true' \
--header 'Content-Type: application/json' \
--data-raw '{
    "question": {
        "properties": {
            "activityKey": {
                "type": "text"
            },
            "activityKeyRaw": {
                "type": "keyword"
            },
            "activityTitle": {
                "type": "text"
            },
            "activityType": {
                "type": "keyword"
            },
            "questionKey": {
                "type": "text"
            },
            "questionKeyRaw": {
                "type": "keyword"
            },
            "tags": {
                "type": "keyword"
            }
        }
    }
}'