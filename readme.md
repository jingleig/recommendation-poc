## How to use:
1. build up the elasticsearch env with `sh local_es_buildup.sh`
2. import the data with `python3 data_importer.py`
3. get the recommendation by `sh recommendation.sh`
## Current model:
1. filter the data with the same `CEFR` and `book` + `unit`
2. match with the same `primary-skill-set` 
3. calcuate the data score with: 
   
   2 * `activity title` + 2 * each `primary-sub-skill` + 0.8 * each `primary-lexical-set` + 1 * `secondary-skill-set` + 0.6 * each `secondary-lexical-set` + 0.5 * other tags
4. score the result with `BM25 TF/IDF` deatil see [https://www.elastic.co/guide/cn/elasticsearch/guide/current/scoring-theory.html]
## Change the recommendation model:
You may change the recommendation model by modifing the `query` object in the `recommendation.sh` `curl` command

## Web service to test the recommendation:

A simple web service is provided to test the recommendation (JAVA and Maven required)

You may start the web service via `sh start_web_service.sh` and stop it by `sh stop_web_service.sh`

A sample API call as following:
```
curl --location --request GET 'localhost:8080/recommend' \
--header 'Content-Type: application/json' \
--data-raw '{
  "activityKey": "highflyers/book-3/unit-3/activities/midterm/multipleselectlongtext-readinga",
  "activityKeyRaw": "highflyers/book-3/unit-3/activities/midterm/multipleselectlongtext-readinga",
  "activityTitle": "Reading A",
  "activityType": "multipleSelectLongText",
  "questionKey": "/content/adam/courses/highflyers/book-3/unit-3/activities/midterm/multipleselectlongtext-readinga/reading-a-/jcr:content/parsys_question/question",
  "questionKeyRaw": "/content/adam/courses/highflyers/book-3/unit-3/activities/midterm/multipleselectlongtext-readinga/reading-a-/jcr:content/parsys_question/question",
  "originTags": {
    "intent": [
      "INTENT:ASSESSMENT",
      "INTENT:ASSESSMENT/A-1"
    ],
    "outcome": [],
    "cefr": [
      "CEFR:A1.1."
    ],
    "age-from": [
      "K-AND-T:AGES",
      "K-AND-T:AGES/7"
    ],
    "age-to": [
      "K-AND-T:AGES",
      "K-AND-T:AGES/10"
    ],
    "primary-skill-set": [
      "K-AND-T:SKILLSET/READING"
    ],
    "primary-sub-skill-reading": [
      "K-AND-T:SUB-SKILLS",
      "K-AND-T:SUB-SKILLS/READING",
      "K-AND-T:SUB-SKILLS/READING/SK-XX-REA-3"
    ],
    "primary-lexical-set-p1": [
      "COMPASS:TO",
      "COMPASS:TO/XX",
      "COMPASS:TO/XX/DAI",
      "COMPASS:TO/XX/DAI/3",
      "COMPASS:TO/XX/DAI/3/2",
      "COMPASS:TO/XX/DAI/8",
      "COMPASS:TO/XX/DAI/8/8",
      "COMPASS:TO/XX/HHE",
      "COMPASS:TO/XX/HHE/3",
      "COMPASS:TO/XX/HHE/3/5",
      "COMPASS:TO/XX/PID",
      "COMPASS:TO/XX/PID/5",
      "COMPASS:TO/XX/PID/5/7"
    ]
  }
}'
```