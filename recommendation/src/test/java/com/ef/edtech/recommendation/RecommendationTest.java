package com.ef.edtech.recommendation;

import com.ef.edtech.kt.recommendation.Application;
import com.ef.edtech.kt.recommendation.elasticsearch.RecommendationService;
import com.ef.edtech.kt.recommendation.elasticsearch.mapping.Question;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = Application.class
)
public class RecommendationTest {

  @Autowired
  private RecommendationService recommendationService;

  @Test
  public void testRecommend() throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    InputStream is = getClass().getClassLoader().getResourceAsStream("question.json");
    Question question = objectMapper.readValue(is, Question.class);
    List<String> recommendations = recommendationService.recommend(question);
    System.out.println(recommendations);
    Assert.assertEquals(5, recommendations.size());
  }
}
