package com.ef.edtech.kt.recommendation.api;

import com.ef.edtech.kt.recommendation.elasticsearch.RecommendationService;
import com.ef.edtech.kt.recommendation.elasticsearch.mapping.Question;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RecommendationController {

  private final RecommendationService recommendationService;

  public RecommendationController(RecommendationService recommendationService) {
    this.recommendationService = recommendationService;
  }

  @GetMapping("/recommend")
  public List<String> recommend(@RequestBody Question question) {
    return recommendationService.recommend(question);
  }
}
