package com.ef.edtech.kt.recommendation.elasticsearch.mapping;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Map;

@Document(indexName = "recommendation", type = "question")
public class Question {

  @Id
  private String id;

  @Field(type = FieldType.Text)
  private String questionKey;
  @Field(type = FieldType.Keyword)
  private String questionKeyRaw;

  @Field(type = FieldType.Text)
  private String activityKey;
  @Field(type = FieldType.Keyword)
  private String activityKeyRaw;

  @Field(type = FieldType.Text)
  private String activityTitle;

  @Field(type = FieldType.Keyword)
  private String activityType;

  @Field(type = FieldType.Object)
  private Map<String, Object> originTags;

  @Field(type = FieldType.Keyword)
  private String[] tags;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getQuestionKey() {
    return questionKey;
  }

  public void setQuestionKey(String questionKey) {
    this.questionKey = questionKey;
  }

  public String getQuestionKeyRaw() {
    return questionKeyRaw;
  }

  public void setQuestionKeyRaw(String questionKeyRaw) {
    this.questionKeyRaw = questionKeyRaw;
  }

  public String getActivityKey() {
    return activityKey;
  }

  public void setActivityKey(String activityKey) {
    this.activityKey = activityKey;
  }

  public String getActivityKeyRaw() {
    return activityKeyRaw;
  }

  public void setActivityKeyRaw(String activityKeyRaw) {
    this.activityKeyRaw = activityKeyRaw;
  }

  public String getActivityTitle() {
    return activityTitle;
  }

  public void setActivityTitle(String activityTitle) {
    this.activityTitle = activityTitle;
  }

  public Map<String, Object> getOriginTags() {
    return originTags;
  }

  public void setOriginTags(Map<String, Object> originTags) {
    this.originTags = originTags;
  }

  public String[] getTags() {
    return tags;
  }

  public void setTags(String[] tags) {
    this.tags = tags;
  }

  public String getActivityType() {
    return activityType;
  }

  public void setActivityType(String activityType) {
    this.activityType = activityType;
  }
}
