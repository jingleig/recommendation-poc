package com.ef.edtech.kt.recommendation.elasticsearch;

import com.ef.edtech.kt.recommendation.elasticsearch.mapping.Question;
import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Lists;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.PrefixQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilterBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RecommendationService {

  private static final String CERF_NODE = "cefr";
  private static final String TAGS = "tags";
  private static final String ACTIVITY_KEY_RAW = "activityKeyRaw";
  private static final String ACTIVITY_TITLE = "activityTitle";
  private static final String PRIMARY_SKILL_SET_NODE = "primary-skill-set";
  private static final String PRIMARY_LEXICAL_SET_NODES = "primary-lexical-set-p1";
  private static final String PRIMARY_SUB_SKILLS_NODE = "primary-sub-skill-.*";
  private static final String SECONDARY_SKILL_SET = "secondary-skill-set";
  private static final String SECONDARY_SUB_SKILLS_NODE = "secondary-sub-skill-.*";
  private static final String SECONDARY_LEXICAL_SET_NODES = "secondary-lexical-set-p2";
  private static final String[] MAJOR_TAGS = {
      CERF_NODE,
      PRIMARY_SKILL_SET_NODE,
      PRIMARY_SUB_SKILLS_NODE,
      PRIMARY_LEXICAL_SET_NODES,
      SECONDARY_SKILL_SET,
      SECONDARY_SUB_SKILLS_NODE,
      SECONDARY_LEXICAL_SET_NODES};

  private final ElasticsearchRestTemplate elasticsearchTemplate;

  @Autowired
  public RecommendationService(ElasticsearchRestTemplate elasticsearchTemplate) {
    this.elasticsearchTemplate = elasticsearchTemplate;
  }

  public List<String> recommend(Question baseQuestion) {
    String activityKeyRaw = baseQuestion.getActivityKeyRaw();
    String cefr = getCEFR(baseQuestion);
    String bookAndUnitPrefix = getBookAndUnitPrefix(activityKeyRaw);
    String primarySkillSet = getPrimarySkillSet(baseQuestion);
    String activityTitle = baseQuestion.getActivityTitle();
    String secondarySkillSet = getSecondarySkillSet(baseQuestion);
    TermQueryBuilder cefrFilter = QueryBuilders.termQuery(TAGS, cefr);
    PrefixQueryBuilder bookAndUnitFilter = QueryBuilders.prefixQuery(ACTIVITY_KEY_RAW, bookAndUnitPrefix);

    BoolQueryBuilder query = QueryBuilders.boolQuery()
        .must(QueryBuilders.termQuery(TAGS, primarySkillSet))
        .should(QueryBuilders.matchQuery(ACTIVITY_TITLE, activityTitle).boost(2.0f))
        .filter(cefrFilter)
        .filter(bookAndUnitFilter);

    List<String> primarySubSkills = getPrimarySubSkills(baseQuestion);
    for (String primarySubSkill : primarySubSkills) {
      query.should(QueryBuilders.termQuery(TAGS, primarySubSkill).boost(2.0f));
    }

    List<String> primaryLexicalSet = getPrimaryLexicalSet(baseQuestion);
    for (String primaryLexical : primaryLexicalSet) {
      query.should(QueryBuilders.termQuery(TAGS, primaryLexical).boost(0.8f));
    }

    if (null != secondarySkillSet) {
      query.should(QueryBuilders.termQuery(TAGS, secondarySkillSet).boost(1.0f));

      List<String> secondarySubSkills = getSecondarySubSkills(baseQuestion);
      for (String secondarySubSkill : secondarySubSkills) {
        query.should(QueryBuilders.termQuery(TAGS, secondarySubSkill).boost(0.8f));
      }
      query.should(QueryBuilders.termQuery(TAGS, secondarySkillSet).boost(1.0f));

      List<String> secondaryLexicalSet = getSecondaryLexicalSet(baseQuestion);
      for (String secondaryLexical : secondaryLexicalSet) {
        query.should(QueryBuilders.termQuery(TAGS, secondaryLexical).boost(0.6f));
      }
    }

    List<String> otherTags = getOtherTags(baseQuestion);
    for (String otherTag : otherTags) {
      query.should(QueryBuilders.termQuery(TAGS, otherTag).boost(0.5f));
    }

    // exclude the same activity
    query.mustNot(QueryBuilders.termQuery(ACTIVITY_KEY_RAW, activityKeyRaw));

//    System.out.println(query);

    NativeSearchQueryBuilder nativeSearchQuery = new NativeSearchQueryBuilder();
    FetchSourceFilterBuilder fetchSourceFilterBuilder = new FetchSourceFilterBuilder();
    fetchSourceFilterBuilder.withIncludes(ACTIVITY_KEY_RAW); // reduce the result size
    nativeSearchQuery.withQuery(query)
        .withSourceFilter(fetchSourceFilterBuilder.build())
        .withCollapseField(ACTIVITY_KEY_RAW);
    NativeSearchQuery searchQuery = nativeSearchQuery.build();
    return elasticsearchTemplate.query(searchQuery, (ResultsExtractor<List>) response -> {
      SearchHits hits = response.getHits();
      if (hits.totalHits > 0) {
        return Arrays.stream(hits.getHits())
            .map(hit -> Optional.ofNullable(hit.getFields().get(ACTIVITY_KEY_RAW))
                .map(doc -> doc.getValue()).orElse(null))
            .collect(Collectors.toList());
      } else {
        return Lists.emptyList();
      }
    });
  }

  private List<String> getOtherTags(Question baseQuestion) {
    return baseQuestion.getOriginTags().entrySet().stream()
        .filter(stringObjectEntry -> {
          for (String key: MAJOR_TAGS) {
            if (stringObjectEntry.getKey().matches(key)) {
              return false;
            }
          }
          return true;
        })
        .map(entity -> entity.getValue())
        .collect(Collectors.toList()).stream()
        .flatMap(object -> ((List<String>)object).stream())
        .collect(Collectors.toList());
  }

  private List<String> getSecondarySubSkills(Question baseQuestion) {
    return getTagValueRegex(baseQuestion, SECONDARY_SUB_SKILLS_NODE).orElse(List.of());
  }

  private List<String> getPrimaryLexicalSet(Question baseQuestion) {
    return getTagValue(baseQuestion, PRIMARY_LEXICAL_SET_NODES).orElse(List.of());
  }

  private List<String> getSecondaryLexicalSet(Question baseQuestion) {
    return getTagValue(baseQuestion, SECONDARY_LEXICAL_SET_NODES).orElse(List.of());
  }

  private List<String> getPrimarySubSkills(Question baseQuestion) {
    return getTagValueRegex(baseQuestion, PRIMARY_SUB_SKILLS_NODE).orElse(List.of());
  }

  private String getSecondarySkillSet(Question baseQuestion) {
    return getTagValue(baseQuestion, SECONDARY_SKILL_SET).map(list -> list.get(0)).orElse(null);
  }

  private String getPrimarySkillSet(Question baseQuestion) {
    return getTagValue(baseQuestion, PRIMARY_SKILL_SET_NODE).orElseThrow(() -> new NoSuchElementException(PRIMARY_SKILL_SET_NODE)).get(0);
  }

  private Optional<List<String>> getTagValueRegex(Question question, String tagRegex) {
    return Optional.ofNullable(question.getOriginTags().entrySet().stream()
        .filter(stringObjectEntry -> stringObjectEntry.getKey().matches(tagRegex))
        .map(entity -> entity.getValue())
        .collect(Collectors.toList()).stream()
          .flatMap(object -> ((List<String>)object).stream())
          .collect(Collectors.toList()));
  }

  private Optional<List<String>> getTagValue(Question question, String tagName) {
    Object tags = question.getOriginTags().get(tagName);
    if (tags instanceof List) {
      return Optional.of((List<String>) tags);
    } else {
      return Optional.empty();
    }
  }

  private String getBookAndUnitPrefix(String activityKey) {
    return activityKey.substring(0, StringUtils.ordinalIndexOf(activityKey, "/", 3));
  }

  private String getCEFR(Question baseQuestion) {
    return getTagValue(baseQuestion, CERF_NODE).orElseThrow(() -> new NoSuchElementException(CERF_NODE)).get(0);
  }
}
